<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
<table>
    <p>List of cheap products</p>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Price</th>
        <th>Img</th>
        <th>Rating</th>
    </tr>
    @foreach($cheapProducts as $product)
        <tr>
            <td>{{ $product['id']}}</td>
            <td>{{ $product['name'] }}</td>
            <td>{{ $product['price'] }}</td>
            <td>{{ $product['img'] }}</td>
            <td>{{ $product['rating'] }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>
