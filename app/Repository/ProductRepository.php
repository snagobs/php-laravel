<?php

declare(strict_types=1);

namespace App\Repository;

class ProductRepository implements ProductRepositoryInterface
{
    protected $products;

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function findAll(): array
    {
        return $this->products;
    }
}
