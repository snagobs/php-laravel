<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetAllProductsAction

{
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;

    }

    public function execute(): GetAllProductsResponse
    {
        $products = $this->productRepository->findAll();

        $response = new GetAllProductsResponse();
        $response->setProducts($products);

        return $response;
    }
}


