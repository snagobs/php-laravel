<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetMostPopularProductResponse
{
    protected $product;

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct($products)
    {
        $this->product = $products;
    }

}

