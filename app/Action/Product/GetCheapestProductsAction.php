<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;

    }

    public function execute(): GetCheapestProductsResponse
    {
        $products = $this->productRepository->findAll();

        usort($products, function (Product $product1, Product $product2) {
            return $product1->getPrice() <=> $product2->getPrice();
        });

        $cheapestProducts = array_slice($products, 0, 3, true);

        $response = new GetCheapestProductsResponse();
        $response->setProducts($cheapestProducts);

        return $response;
    }
}
