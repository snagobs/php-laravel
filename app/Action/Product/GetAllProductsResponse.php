<?php

declare(strict_types=1);

namespace App\Action\Product;


class GetAllProductsResponse
{
    protected $products;

    public function getProducts()
    {
        return $this->products;
    }

    public function setProducts($products)
    {
        $this->products = $products;
    }

}


