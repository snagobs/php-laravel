<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;

    }

    public function execute(): GetMostPopularProductResponse
    {
        $products = $this->productRepository->findAll();

        usort($products, function(Product $product1, Product $product2) {
            return -1 * ($product1->getRating() <=> $product2->getRating());
        });

        $response = new GetMostPopularProductResponse();
        $response->setProduct($products[0]);

        return $response;
    }
}
