<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    public static function generate(): array
    {
        return [
            new Product(1, 'product1', 100.11, 'url1', 1.1),
            new Product(2, 'product2', 200.22, 'url2', 2.2),
            new Product(3, 'product3', 300.33, 'url3', 3.3),
            new Product(4, 'product4', 400.44, 'url4', 4.4),
        ];

    }
}
