<?php

namespace App\Http\Controllers;

use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;
use App\Services\ProductGenerator;

class ProductController extends Controller
{
    public function getMostPopularProduct(GetMostPopularProductAction $product)
    {
        return ProductArrayPresenter::present($product->execute()->getProduct());
    }

    public function getProducts()
    {
        return ProductArrayPresenter::presentCollection(ProductGenerator::generate());
    }

    public function getCheapProduct(GetCheapestProductsAction $products)
    {
         return view('cheap_products', ['cheapProducts' => ProductArrayPresenter::presentCollection($products->execute()->getProducts())]);
    }
}
