<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Product;

class ProductArrayPresenter
{
     /**
     * @param Product[] $products
     * @return array
     */
    public static function presentCollection(array $products): array
    {
        $newProducts = [];

        foreach ($products as $product)
        {
            $newProducts[] = [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'img' => $product->getImageUrl(),
                'rating' => $product->getRating()
            ];
        }

        return $newProducts;
    }

    public static function present(Product $product): array
    {
        return $newProduct = [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'img' => $product->getImageUrl(),
            'rating' => $product->getRating()
        ];
    }
}

